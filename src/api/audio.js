import request from '@/utils/request'

export function getAudios(queryParam) {
  return request({
    url: '/uav-navigation-service/audioList',
    method: 'post',
    data: queryParam
  })
}

export function getAudioTags() {
  return request({
    url: '/uav-navigation-service/audio/tags',
    method: 'get'
  })
}

export function submitAudio(data) {
  return request({
    url: '/uav-navigation-service/audio',
    method: 'post',
    data
  })
}

export function getAudio() {
  return request({
    url: '/uav-navigation-service/audios',
    method: 'get'
  })
}

export function deleteAudio(params) {
  return request({
    url: '/uav-navigation-service/audio',
    method: 'delete',
    params
  })
}
