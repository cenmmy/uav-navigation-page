import request from '@/utils/request'

//查询无人机不分页
export function getDroneListNoPage(queryParam) {
  return request({
    url: '/uav-navigation-service/drone/queryAllDroneNoPage',
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(queryParam)
  })
}
//查询无人机
export function getDroneList(queryParam) {
  return request({
    url: '/uav-navigation-service/drone/queryAllDrone',
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(queryParam)
  })
}
//修改无人机
export function updateDrone(data) {
  return request({
    url: '/uav-navigation-service/drone/updateDrone',
    method: 'post',
    data: data
  })
}
//添加无人机
export function addDrone(data) {
  return request({
    url: '/uav-navigation-service/drone/addDrone',
    method: 'post',
    data: data
  })
}
//删除无人机
export function deleteDrone(droneId) {
  return request({
    url: '/uav-navigation-service/drone/deleteDrone',
    method: 'delete',
    params: {
      droneId: droneId
    }
  })
}
