import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/uav-navigation-service/vue-admin-template/table/list',
    method: 'get',
    params
  })
}
