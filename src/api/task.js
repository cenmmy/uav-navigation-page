import request from '@/utils/request'

export function getTaskList(queryParam) {
  return request({
    url: '/uav-navigation-service/task/list',
    method: 'post',
    data: queryParam
  })
}

export function submitTask(data) {
  return request({
    url: '/uav-navigation-service/task',
    method: 'post',
    data
  })
}
export function editTask(data) {
  return request({
    url: '/uav-navigation-service/task/edit',
    method: 'post',
    data
  })
}
export function deleteTask(taskId) {
  return request({
    url: '/uav-navigation-service/task?id=' + taskId,
    method: 'delete',
  })
}
