import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken, setToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// 请求拦截器，在每个请求发送之前调用
service.interceptors.request.use(
  config => {
    // 判断vuex中是否存在token
    if (store.getters.token) {
      // 如果存在token，则从本地cookie中取出该token
      config.headers['token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// 响应拦截器，每收到一个响应之后，将响应中的token保存到本地，以刷新本地token
service.interceptors.response.use(
  // 收到响应之后刷新本地token
  response => {
    const header = response.headers
    const res = response.data

      if (res.code == 50008 || res.code == 50009) {
        MessageBox.confirm('您已经登出，您可以离开当前页面或者重新登录', '确认登出', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
        return Promise.reject(new Error(res.msg || 'Error'))
      } else {
      // 当token刷新时，重新设置token
      console.log(response)
      console.log(header.hasOwnProperty('refreshtoken'))
      if (header.hasOwnProperty('refreshtoken')) {
        console.log('刷新本地token')
        store.commit('user/SET_TOKEN', header.refreshtoken)
        setToken(header.refreshtoken)
      }
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
