/**
 * index.vue
 */
//任务列表
const queryParam = {
  pageNum: 1,
  pageSize: 10,
  startTime: "",
  endTime: "",
  params: {
    taskName: null,
    state: null,
    droneDTO: {
      id: null
    },
    audioDto: {
      id: null
    },
    goRouteId: null,
    backRouteId: null
  },
}
const options = [{
    value: "0",
    label: "未执行",
  },
  {
    value: "1",
    label: "执行中",
  },
  {
    value: "2",
    label: "已执行",
  },
]
/**
 * el-diaog
 */
const rules = {
  taskName: [{
    required: true,
    message: "请输入任务名称",
    trigger: "blur"
  }, ],
  audioId: [{
    required: true,
    message: "请选择音频",
    trigger: "blur"
  }],
  droneId: [{
    required: true,
    message: "请选择无人机",
    trigger: "blur"
  }],
  goRouteId: [{
    required: true,
    message: "请选择前往航线",
    trigger: "blur"
  }, ],
  backRouteId: [{
    required: true,
    message: "请选择返回航线",
    trigger: "blur"
  }, ],
}

const taskInfoForm = {
  id: "",
  taskName: "",
  audioId: "",
  droneId: "",
  goRouteId: "",
  backRouteId: "",
  scheduledExecuteTime: "",
}

const audio = {
  audioName: null,
  audioUrl: null,
  audioTags: null,
  audioDuration: null,
}
export {
  queryParam,
  options,
  taskInfoForm,
  rules,
  audio,
};
